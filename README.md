# movieguru

Welcome!

# Deployed Application

You can find deployed application in `https://secret-inlet-23858.herokuapp.com/api/`

# Instalation

In order to run application:
1. create postgres database using credentials that you can find in settings.base file.
    ```
    database name: 'movieguru',
    username: 'movieguruuser',
    password: 'movieguruuser',
    ```
2. install dependencies from requirements file `pip install -r requirements/local.txt`
3. make migrations `python manage.py migrate`
4. Run application! `DJANGO_SETTINGS_MODULE=movieshelf.settings.dev python manage.py runserver`

