from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from api.models.movie import Movie
import json
import os
from api.serializers.movies import MovieSerializer
from api.tests.test_class import MovieGuruTestClass


class MovieBaseTest(APITestCase, MovieGuruTestClass):
    client = APIClient()

    @classmethod
    def setUpClass(self):
        if Movie.objects.all().count() == 0:
            data = MovieBaseTest.fetch_database_objects(
                os.path.abspath('api/tests/movie_test_batch.json'))
            for i in data:
                serializer = MovieSerializer(data=i)
                if serializer.is_valid():
                    serializer.save()

    @classmethod
    def tearDownClass(cls):
        pass


class MoviesListTest(MovieBaseTest):

    def test_get_movie_list_200(self):
        response = self.client.get(
            reverse("movies-list"),
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_post_201(self):
        response = self.client.post(
            reverse('movies-list'), {'title': 'Harry Potter'}
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_post_404_error_in_external_api(self):
        response = self.client.post(
            reverse('movies-list'), {'title': 'notatitle'}
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_post_400_object_is_in_database(self):
        response = self.client.post(
            reverse('movies-list'), {'title': 'Titanic'}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class MovieDetailTest(MovieBaseTest):

    def test_get_200(self):
        response = self.client.get(
            reverse('movies-detail', kwargs={'pk': 1}),
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_404(self):
        response = self.client.get(
            reverse('movies-detail', kwargs={'pk': 10}),
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_put_200(self):
        old_object = self.client.get(
            reverse('movies-detail', kwargs={'pk': 1}),
        )
        old_object.data['title'] = "LALALA"

        response = self.client.put(
            reverse('movies-detail', kwargs={'pk': 1}), old_object.data
        )
        self.assertEqual(response.data['title'], "LALALA")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_204(self):
        response = self.client.delete(
            reverse('movies-detail', kwargs={'pk': 2})
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
