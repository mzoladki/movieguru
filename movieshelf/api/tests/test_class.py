import json


class MovieGuruTestClass():

    @staticmethod
    def fetch_database_objects(filename):
        with open(filename, 'r') as f:
            data = json.loads(f.read())
        return data
