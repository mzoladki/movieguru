from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from api.models.comment import Comment
from api.models.movie import Movie
import json
import os
from api.serializers.comments import CommentSerializer
from api.serializers.movies import MovieSerializer
from api.tests.test_class import MovieGuruTestClass


class TopBaseTest(APITestCase, MovieGuruTestClass):
    client = APIClient()
    database_created = False

    @classmethod
    def setUpClass(cls):
        if Movie.objects.all().count() == 0:
            movies = TopBaseTest.fetch_database_objects(
                os.path.abspath('api/tests/movie_test_batch.json'))
            comments = TopBaseTest.fetch_database_objects(
                os.path.abspath('api/tests/comment_test_batch.json'))
            for movie in movies:
                serializer = MovieSerializer(data=movie)
                if serializer.is_valid():
                    serializer.save()

            for comment in comments:
                serializer = CommentSerializer(data=comment)
                if serializer.is_valid():
                    serializer.save()

    @classmethod
    def tearDownClass(cls):
        pass


class CommentListTest(TopBaseTest):

    def test_get_top_200(self):
        response = self.client.get(
            reverse("top-list")
        )
        self.assertEqual(response.data[0]['rank'], 1)
        self.assertEqual(response.data[-1]['rank'], 3)
        self.assertEqual(response.data[1]['rank'], response.data[2]['rank'])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
