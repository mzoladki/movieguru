import json
import os
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from api.models.movie import Movie
from api.serializers.comments import CommentSerializer
from api.serializers.movies import MovieSerializer
from api.tests.test_class import MovieGuruTestClass


class CommentBaseTest(APITestCase, MovieGuruTestClass):
    client = APIClient()
    database_created = False

    @classmethod
    def setUpClass(cls):
        if Movie.objects.all().count() == 0:
            movies = CommentBaseTest.fetch_database_objects(
                os.path.abspath('api/tests/movie_test_batch.json'))
            comments = CommentBaseTest.fetch_database_objects(
                os.path.abspath('api/tests/comment_test_batch.json'))
            for movie in movies:
                serializer = MovieSerializer(data=movie)
                if serializer.is_valid():
                    serializer.save()

            for comment in comments:
                serializer = CommentSerializer(data=comment)
                if serializer.is_valid():
                    serializer.save()

    @classmethod
    def tearDownClass(cls):
        pass


class CommentListTest(CommentBaseTest):

    def test_get_comment_list_200(self):
        response = self.client.get(
            reverse("comments-list")
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_comment_list_404(self):
        response = self.client.get(
            reverse('comments-list'), {'movieID': 100}
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_post_201(self):
        response = self.client.post(
            reverse('comments-list'), {'movie': 1,
                                       'text': 'Thats new comment!'}
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_post_400_movie_does_not_exist(self):
        response = self.client.post(
            reverse('comments-list'), {'movie': 100,
                                       'text': 'Thats new comment to movie that does not exist!'}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_post_400_object_is_in_database(self):
        response = self.client.post(
            reverse('comments-list'), {'movie': 1, 'text': ''}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class CommentDetailTest(CommentBaseTest):

    def test_get_200(self):
        response = self.client.get(
            reverse('comments-detail', kwargs={'pk': 1}),
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_404(self):
        response = self.client.get(
            reverse('comments-detail', kwargs={'pk': 20}),
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_put_200(self):
        old_object = self.client.get(
            reverse('comments-detail', kwargs={'pk': 1}),
        )
        response = self.client.put(
            reverse('comments-detail', kwargs={'pk': 1}), {
                'movie': 2, 'text': 'Changing comment movie and text!'}
        )

        self.assertNotEqual(old_object.data['movie'], response.data['movie'])
        self.assertEqual(response.data['movie'], 2)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_204(self):
        response = self.client.delete(
            reverse('comments-detail', kwargs={'pk': 1})
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
