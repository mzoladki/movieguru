from django.contrib import admin
from api.models.comment import Comment
from api.models.movie import Movie

admin.site.register(Comment)
admin.site.register(Movie)
