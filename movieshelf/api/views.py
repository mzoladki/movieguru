from datetime import datetime
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import status
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from django.shortcuts import get_list_or_404
from django.db.models import Count
from django.db.models.expressions import Window, F
from django.db.models.functions.window import DenseRank
from django.conf import settings
from api.models.comment import Comment
from api.models.movie import Movie
from api.serializers.comments import CommentSerializer
from api.serializers.movies import MovieSerializer
from api.external_api_handler import get_to_omdb_api


class MovieViewSet(viewsets.ModelViewSet):

    queryset = Movie.objects.all()
    serializer_class = MovieSerializer

    def create(self, request):
        title = request.data['title']
        external_data = get_to_omdb_api(title)

        post_data = {
            'title': external_data['Title'],
            'year': external_data['Year'],
            'rated': external_data['Rated'],
            'released': datetime.strptime(external_data['Released'], '%d %b %Y').date() if external_data['Released'] != 'N/A' else None,
            'runtime': int(external_data['Runtime'].split(' ')[0]) if external_data['Runtime'] != 'N/A' else None,
            'genre': external_data['Genre'].split(', '),
            'director': external_data['Director'],
            'writer': external_data['Writer'],
            'actors': external_data['Actors'].split(', '),
            'plot': external_data['Plot'],
            'language': external_data['Language'].split(', '),
            'country': external_data['Country'].split(', '),
            'awards': external_data['Awards'],
            'ratings': ["{}: {}".format(rate['Source'], rate['Value']) for rate in external_data['Ratings']],
            'metascore': int(external_data['Metascore']) if external_data['Metascore'] != 'N/A' else None,
            'imdb_rating': float(external_data['imdbRating']) if external_data['imdbRating'] != 'N/A' else None,
            'imdb_votes': int(external_data['imdbVotes'].replace(',', '')) if external_data['imdbVotes'] != 'N/A' else None,
            'imdb_id': external_data['imdbID'],
            'movie_type': external_data['Type'],
            'dvd': datetime.strptime(external_data['DVD'], '%d %b %Y').date() if external_data['DVD'] != 'N/A' else None,
            'box_office': int(external_data['BoxOffice'].replace(',', '').replace('$', '')) if external_data['BoxOffice'] != 'N/A' else None,
            'production': external_data['Production']
        }
        serializer = self.serializer_class(data=post_data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CommentsViewSet(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

    @swagger_auto_schema(title='Comment', manual_parameters=[openapi.Parameter(
        'movieID', openapi.IN_QUERY, description="movie id", type=openapi.TYPE_INTEGER
    )])
    def list(self, request):
        query_params = request.query_params.get('movieID', None)
        if query_params:
            self.queryset = get_list_or_404(Comment, movie=query_params)
        else:
            self.queryset = get_list_or_404(Comment)

        return super(CommentsViewSet, self).list(self, request)


class TopView(APIView):

    model = Movie

    dense_rank = Window(
        expression=DenseRank(),
        order_by=F('total_comments').desc()
    )

    def get(self, request, format=None):
        movies = self.model.objects.annotate(total_comments=Count('comment')).order_by(
            '-total_comments').annotate(rank=self.dense_rank)
        movies = [
            {
                'movie_id': movie.pk,
                'total_comments': movie.total_comments,
                'rank': movie.rank
            } for movie in movies
        ]
        return Response(movies)
