from django.db import models
from api.models.movie import Movie


class Comment(models.Model):
    text = models.TextField()
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)

    def __str__(self):
        return "pk.{} movie id.{}, text: {}".format(self.pk, self.movie.id, self.text)
