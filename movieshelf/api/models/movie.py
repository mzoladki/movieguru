from django.contrib.postgres.fields import ArrayField
from django.db import models
import datetime


def year_choices():
    return [(r, r) for r in range(1900, datetime.date.today().year+1)]


class Movie(models.Model):
    title = models.CharField(max_length=255, null=True, blank=False)
    year = models.IntegerField(choices=year_choices(), null=True, blank=False)
    rated = models.CharField(max_length=20, null=True, blank=False)
    released = models.DateField(null=True, blank=False)
    runtime = models.IntegerField(null=True, blank=False)
    genre = ArrayField(models.CharField(max_length=100, null=True, blank=False))
    director = models.CharField(max_length=100, null=True, blank=False)
    writer = models.TextField(null=True, blank=False)
    actors = ArrayField(models.CharField(max_length=100, null=True, blank=False))
    plot = models.TextField(null=True, blank=False)
    language = ArrayField(models.CharField(max_length=100, null=True, blank=False))
    country = ArrayField(models.CharField(max_length=100, null=True, blank=False))
    awards = models.TextField(null=True, blank=False)
    ratings = ArrayField(models.CharField(max_length=100, null=True, blank=False))
    metascore = models.IntegerField(null=True, blank=False)
    imdb_rating = models.FloatField(null=True, blank=False)
    imdb_votes = models.IntegerField(null=True, blank=False)
    imdb_id = models.CharField(max_length=150, null=True, blank=False)
    movie_type = models.CharField(max_length=100, null=True, blank=False)
    dvd = models.DateField(null=True, blank=False)
    box_office = models.IntegerField(null=True, blank=False)
    production = models.CharField(max_length=100, null=True, blank=False)

    def __str__(self):
        return "{}. {}, year: {}".format(self.pk, self.title, self.year)

    class Meta:
        unique_together = ['title']

