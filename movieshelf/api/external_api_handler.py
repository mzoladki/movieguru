import json
import requests
from rest_framework.response import Response
from django.conf import settings
from django.http import Http404


def get_to_omdb_api(title):
    payload = {'t': title, "apikey": settings.OMDB_API_KEY}
    external_data = json.loads(requests.get(
        "http://www.omdbapi.com", params=payload).text)

    if 'Error' in external_data.keys():
        raise Http404
    else:
        return external_data
