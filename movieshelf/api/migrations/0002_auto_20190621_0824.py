# Generated by Django 2.2.2 on 2019-06-21 08:24

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='movie',
            unique_together={('title',)},
        ),
    ]
