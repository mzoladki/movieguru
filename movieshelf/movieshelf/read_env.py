import json

def set_variable_from_env_file(conf_file, variable_name):
    with open(conf_file, 'r') as f:
        config_file = json.loads(f.read())
    return config_file[variable_name]